<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateShippingPackagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shipping_packages', function (Blueprint $table) {
            $table->id();
            $table->integer('order_number');
            $table->enum('delivery_company', config('app.company_list'));
            $table->string('awb', 255);
            $table->integer('weight')->nullable();
            $table->float('package_value', 8, 3);
            $table->dateTime('date_dispatched')->nullable();
            $table->dateTime('date_delivered')->nullable();
            $table->dateTime('date_return_received')->nullable();
            $table->boolean('customer_rejected_shipment')->nullable();
            $table->boolean('courier_company_lost_shipment')->nullable();
            $table->boolean('returned_other_reason')->nullable();
            $table->foreignId('created_by');
            $table->foreignId('processed_by')->nullable();
            $table->timestamps();
            $table->foreign('created_by')->references('id')->on('users');
            $table->foreign('processed_by')->references('id')->on('users');
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shipping_packages');
    }
}
