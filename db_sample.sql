-- MySQL dump 10.13  Distrib 5.7.31, for Linux (x86_64)
--
-- Host: localhost    Database: shipment_packages
-- ------------------------------------------------------
-- Server version	5.7.31-0ubuntu0.18.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `failed_jobs`
--

DROP TABLE IF EXISTS `failed_jobs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `failed_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `failed_jobs`
--

LOCK TABLES `failed_jobs` WRITE;
/*!40000 ALTER TABLE `failed_jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `failed_jobs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2014_10_12_000000_create_users_table',1),(2,'2014_10_12_100000_create_password_resets_table',2),(28,'2016_06_01_000001_create_oauth_auth_codes_table',3),(29,'2016_06_01_000002_create_oauth_access_tokens_table',3),(30,'2016_06_01_000003_create_oauth_refresh_tokens_table',3),(31,'2016_06_01_000004_create_oauth_clients_table',3),(32,'2016_06_01_000005_create_oauth_personal_access_clients_table',3),(33,'2019_08_19_000000_create_failed_jobs_table',3),(34,'2020_10_11_151556_create_shipping_packages_table',3);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_access_tokens`
--

DROP TABLE IF EXISTS `oauth_access_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) unsigned DEFAULT NULL,
  `client_id` bigint(20) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_access_tokens_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_access_tokens`
--

LOCK TABLES `oauth_access_tokens` WRITE;
/*!40000 ALTER TABLE `oauth_access_tokens` DISABLE KEYS */;
INSERT INTO `oauth_access_tokens` VALUES ('11c378fca171013c1d1a7856a1f724a42405e4a74fc430454e66c90682ea47bbf84506dc2fcf3684',1,1,'Personal Access Token','[]',0,'2020-10-11 11:35:13','2020-10-11 11:35:13','2020-10-12 17:05:13'),('19c26916947d24475496a7872d254f85b2ac60762a8e05345f468e77798f022d84cc0b3d0853ce30',1,1,'Personal Access Token','[]',0,'2020-10-11 13:16:59','2020-10-11 13:16:59','2020-10-12 18:46:58'),('4499f237babf5063e3f9f9738c17ddb2762a77ab2b1aef7803033f88aecfc544e6638959c53837de',1,1,'Personal Access Token','[]',0,'2020-10-11 13:13:15','2020-10-11 13:13:15','2020-10-12 18:43:15'),('62bdbca3f6dbfb811f46b4392baa1d9581108561adb8f14236e2d354062a28fba8b87385a7dd40f1',1,1,'Personal Access Token','[]',0,'2020-10-11 11:32:49','2020-10-11 11:32:49','2021-10-11 17:02:49'),('6d0f7db61feead256b300f24a1b241c52bfc33b14ded585369ec3177bb0add3d18155ffc3543e17c',1,1,'Personal Access Token','[]',0,'2020-10-11 13:14:10','2020-10-11 13:14:10','2020-10-12 18:44:10'),('85bb188e8eb7e860ea28634b02b0e516e131d143c493a8fad279e16b6289924af9a0fd64229f4016',1,1,'Personal Access Token','[]',0,'2020-10-11 11:35:04','2020-10-11 11:35:04','2020-10-12 17:05:03'),('8e4f31e8f60a545a391ff137bcf2cf9437956919dbcbc2fccac4783240c93174971c39af0f4e29b2',1,1,'Personal Access Token','[]',0,'2020-10-11 11:31:48','2020-10-11 11:31:48','2021-10-11 17:01:48'),('9e8a713f39ba3956a633a52e4a70de421703ca78d24ee78184e1bf6ee97c740fa4c30dae2857beb9',1,1,'Personal Access Token','[]',0,'2020-10-11 13:16:33','2020-10-11 13:16:33','2020-10-12 18:46:33'),('ccb28ca6c56bab0ad2c262a3d67ddda91834905cf748d83cfecbfc43cb4ffaa82157c0666ac2f5f7',1,1,'Personal Access Token','[]',0,'2020-10-12 12:21:16','2020-10-12 12:21:16','2020-10-13 17:51:06'),('df8d15749ce25faca6cf1c617d5af1a9c7d051b7f29b3ede4b055f0b08ec110f314e58fdcfb066ed',1,1,'Personal Access Token','[]',0,'2020-10-11 13:16:45','2020-10-11 13:16:45','2020-10-12 18:46:45'),('e619b1c3fcf9c9f1313b0bd122363ccfc22bc2ab08ee518b22596e72176778066cb96015582c7944',1,1,'Personal Access Token','[]',0,'2020-10-11 13:17:14','2020-10-11 13:17:14','2020-10-12 18:47:13');
/*!40000 ALTER TABLE `oauth_access_tokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_auth_codes`
--

DROP TABLE IF EXISTS `oauth_auth_codes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) unsigned NOT NULL,
  `client_id` bigint(20) unsigned NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_auth_codes_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_auth_codes`
--

LOCK TABLES `oauth_auth_codes` WRITE;
/*!40000 ALTER TABLE `oauth_auth_codes` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_auth_codes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_clients`
--

DROP TABLE IF EXISTS `oauth_clients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_clients` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_clients_user_id_index` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_clients`
--

LOCK TABLES `oauth_clients` WRITE;
/*!40000 ALTER TABLE `oauth_clients` DISABLE KEYS */;
INSERT INTO `oauth_clients` VALUES (1,NULL,'Laravel Personal Access Client','xM2y1aPLLzvuUAAxolQB0ETdf3LE6RDUl1H0NwSV',NULL,'http://localhost',1,0,0,'2020-10-11 11:31:11','2020-10-11 11:31:11'),(2,NULL,'Laravel Password Grant Client','qBfGFiJru4azSvrAUvyAXXz26xHKPDuLxw8mrRtp','users','http://localhost',0,1,0,'2020-10-11 11:31:11','2020-10-11 11:31:11');
/*!40000 ALTER TABLE `oauth_clients` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_personal_access_clients`
--

DROP TABLE IF EXISTS `oauth_personal_access_clients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_personal_access_clients` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `client_id` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_personal_access_clients`
--

LOCK TABLES `oauth_personal_access_clients` WRITE;
/*!40000 ALTER TABLE `oauth_personal_access_clients` DISABLE KEYS */;
INSERT INTO `oauth_personal_access_clients` VALUES (1,1,'2020-10-11 11:31:11','2020-10-11 11:31:11');
/*!40000 ALTER TABLE `oauth_personal_access_clients` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_refresh_tokens`
--

DROP TABLE IF EXISTS `oauth_refresh_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_refresh_tokens`
--

LOCK TABLES `oauth_refresh_tokens` WRITE;
/*!40000 ALTER TABLE `oauth_refresh_tokens` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_refresh_tokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shipping_packages`
--

DROP TABLE IF EXISTS `shipping_packages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shipping_packages` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `order_number` int(11) NOT NULL,
  `delivery_company` enum('Blue Dart','FedEx','DHL','Shree Maruti','DTDC','India Post') COLLATE utf8mb4_unicode_ci NOT NULL,
  `awb` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `weight` int(11) DEFAULT NULL,
  `package_value` double(8,3) NOT NULL,
  `date_dispatched` datetime DEFAULT NULL,
  `date_delivered` datetime DEFAULT NULL,
  `date_return_received` datetime DEFAULT NULL,
  `customer_rejected_shipment` tinyint(1) DEFAULT NULL,
  `courier_company_lost_shipment` tinyint(1) DEFAULT NULL,
  `returned_other_reason` tinyint(1) DEFAULT NULL,
  `created_by` bigint(20) unsigned NOT NULL,
  `processed_by` bigint(20) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `shipping_packages_created_by_foreign` (`created_by`),
  KEY `shipping_packages_processed_by_foreign` (`processed_by`),
  CONSTRAINT `shipping_packages_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
  CONSTRAINT `shipping_packages_processed_by_foreign` FOREIGN KEY (`processed_by`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shipping_packages`
--

LOCK TABLES `shipping_packages` WRITE;
/*!40000 ALTER TABLE `shipping_packages` DISABLE KEYS */;
INSERT INTO `shipping_packages` VALUES (1,1234,'FedEx','aew12345',20850,20.850,NULL,NULL,NULL,1,0,1,1,1,'2020-10-11 14:26:25','2020-10-11 14:42:48','2020-10-11 14:42:48'),(2,1234,'FedEx','aew123456',20850,20.850,NULL,NULL,NULL,1,0,1,1,1,'2020-10-11 23:36:36','2020-10-11 23:36:36',NULL),(3,1234,'FedEx','aew1234567',20850,20.850,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,'2020-10-11 23:33:28','2020-10-11 23:33:28',NULL),(4,1234,'FedEx','aew12345678',20850,20.850,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,'2020-10-11 23:33:55','2020-10-11 23:33:55',NULL),(5,1234,'FedEx','aew12345679',20850,20.850,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,'2020-10-11 23:34:04','2020-10-11 23:34:04',NULL),(6,1234,'FedEx','aew123456791',20850,20.850,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,'2020-10-11 23:37:28','2020-10-11 23:37:28',NULL),(7,1234,'FedEx','aew123456781',20850,20.850,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,'2020-10-11 23:38:19','2020-10-11 23:38:19',NULL),(8,1234,'FedEx','aew123456782',20850,20.850,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,'2020-10-12 03:13:42','2020-10-12 03:13:42',NULL),(9,1234,'FedEx','aew1234567822',20850,20.850,NULL,NULL,NULL,1,0,1,1,1,'2020-10-12 12:24:45','2020-10-12 12:24:45',NULL);
/*!40000 ALTER TABLE `shipping_packages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `api_token` varchar(80) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`),
  UNIQUE KEY `users_api_token_unique` (`api_token`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Admin','nitishjha1712@gmail.com',NULL,'$2y$10$eMc7C1PKJ3vdRKGv8rTSOObYjmt2jrV/NhZ4Elgi94zD0N4YDuBwK',NULL,NULL,'2020-10-11 06:12:51','2020-10-11 06:12:51');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-10-12 23:40:49
