<?php

namespace App\Exceptions;

use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Throwable  $exception
     * @return void
     *
     * @throws \Throwable
     */
    public function report(Throwable $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Throwable  $exception
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws \Throwable
     */
    public function render($request, Throwable $exception)
    {
        if ($exception instanceof \Illuminate\Validation\ValidationException) {
            return failed('The given data is invalid.', 422, $exception->errors());
        } elseif ($exception instanceof \Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException) {
            return failed('Method not allowed', 405);
        } elseif ($exception instanceof \Illuminate\Auth\AuthenticationException) {
            return failed('Unauthenticated access', 401);
        } elseif ($exception instanceof \Illuminate\Auth\Access\AuthorizationException) {
            return failed('You are not authorized for this action.', 403);
        } elseif ($exception instanceof \Illuminate\Database\Eloquent\ModelNotFoundException || $exception instanceof \Symfony\Component\HttpKernel\Exception\NotFoundHttpException || $exception instanceof \Symfony\Component\Routing\Exception\RouteNotFoundException) {
            return failed('Record Not found.', 404);
        } elseif ($exception instanceof \Illuminate\Http\Exceptions\ThrottleRequestsException) {
            return failed('Too many attempts', 429);
        } elseif ($exception instanceof \Symfony\Component\HttpKernel\Exception\HttpException) {
            return failed($exception->getStatusCode(), $exception->getMessage());
        } else {
            $appEnvironment = app()->environment();

            if ($appEnvironment != 'local') {
                $this->sendEmail($exception);
            }

            return failed('Server error', 500);
        }
        return parent::render($request, $exception);
    }
}
