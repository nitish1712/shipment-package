<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule; 

class ShippingPackageRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'order_number' => 'required|integer',
            'delivery_company' => 'required|in:'.implode(',', config('app.company_list')),
            'awb' => 'required|unique:shipping_packages,awb|max:255',
            'weight' => 'required|integer',
            'package_value' => 'required|between:0,99999999.999',
            'customer_rejected_shipment' => 'nullable|boolean',
            'courier_company_lost_shipment' => 'nullable|boolean',
            'returned_other_reason' => 'nullable|boolean',
            'date_dispatched' => 'nullable|date_format:Y-m-d H:i:s',
            'date_delivered' => 'nullable|date_format:Y-m-d H:i:s',
            'date_return_received' => 'nullable|date_format:Y-m-d H:i:s'
        ];

        if (in_array($this->method(), ['PUT', 'PATCH'])) {

            $rules['awb'] = 'required|unique:shipping_packages,awb,'.$this->package.'|max:255';
        }
        return $rules;
    }
}
