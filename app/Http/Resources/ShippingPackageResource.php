<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\User;

class ShippingPackageResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'order_number' => $this->order_number,
            'delivery_company' => $this->delivery_company,
            'awb' => $this->awb,
            'weight' => $this->weight,
            'package_value' => $this->package_value,
            'date_created' => $this->date_created,
            'date_dispatched' => $this->date_dispatched,
            'date_delivered' => $this->date_delivered,
            'date_return_received' => $this->date_return_received,
            'customer_rejected_shipment' => $this->getBooleanValue($this->customer_rejected_shipment),
            'courier_company_lost_shipment' => $this->getBooleanValue($this->courier_company_lost_shipment),
            'returned_other_reason' => $this->getBooleanValue($this->returned_other_reason),
            'created_by' => $this->getUser($this->created_by),
            'processed_by' => !empty($this->processed_by) ? $this->getUser($this->processed_by) : null
        ];
    }

    public function getUser($id){
        $user = User::select('name')->find($id);
        return $user->name;
    }

    public function getBooleanValue($value){
        if($value === 1){
            return 'true';
        } else if($value === 0){
            return 'false';
        } else{
            return null;
        }
    }
}
