<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ShippingPackageListResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'order_number' => $this->order_number,
            'delivery_company' => $this->delivery_company,
            'awb' => $this->awb,
            'weight' => $this->weight,
            'package_value' => $this->package_value,
            'date_created' => $this->date_created
        ];
    }
}
