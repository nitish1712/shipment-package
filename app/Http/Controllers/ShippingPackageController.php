<?php

namespace App\Http\Controllers;

use App\Http\Requests\ShippingPackageRequest;
use App\Http\Resources\ShippingPackageResource;
use App\Http\Resources\ShippingPackageListResource;
use App\ShippingPackage;
use Illuminate\Http\Request;

class ShippingPackageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try{
            return ShippingPackageListResource::collection(ShippingPackage::paginate());
        } catch(\Exception $e){
            throw $e;
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ShippingPackageRequest $request)
    {
        try{
            $request = $request->validated();
            $package = ShippingPackage::create([
                'order_number' => $request['order_number'],
                'delivery_company' => $request['delivery_company'],
                'awb' => $request['awb'],
                'weight' => $request['weight'],
                'package_value' => $request['package_value'],
                'created_by' => $this->getUser(),
                'created_at' => now(),
                'updated_at' => now()
            ]);
            return success("package created successfully", 201, ["id" => $package->id, "order_number" => $package->order_number]);
        } catch(\Exception $e){
            throw $e;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        try{    
            return ShippingPackageResource::collection(ShippingPackage::where('id', $id)->get());
        } catch(\Exception $e){
            throw $e;
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        dd("Inside edit controller");
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ShippingPackageRequest $request, $id)
    {
        try{
            $request = $request->validated();
            ShippingPackage::find($id)->update([
                'order_number' => $request['order_number'],
                'delivery_company' => $request['delivery_company'],
                'awb' => $request['awb'],
                'weight' => $request['weight'],
                'package_value' => $request['package_value'],
                'created_by' => $this->getUser(),
                'date_dispatched' => $request['date_dispatched'],
                'date_delivered' => $request['date_delivered'],
                'date_return_received' => $request['date_return_received'],
                'customer_rejected_shipment' => $request['customer_rejected_shipment'],
                'courier_company_lost_shipment' => $request['courier_company_lost_shipment'],
                'returned_other_reason' => $request['returned_other_reason'],
                'processed_by' => $this->getUser(),
                'created_at' => now(),
                'updated_at' => now()
            ]);
            return success("package updated successfully", 200, ["id" => $id, "order_number" => $request['order_number']]);
        } catch(\Exception $e){
            throw $e;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            return ShippingPackage::where('id', $id)->delete();
        } catch(\Exception $e){
            throw $e;
        }
    }

    public function getUser(){
        return \Auth::user()->id;
    }
}
