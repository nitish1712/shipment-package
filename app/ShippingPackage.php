<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ShippingPackage extends Model
{
	use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['order_number', 'delivery_company', 'awb', 'weight', 'package_value', 'date_dispatched', 'date_delivered', 'date_return_received', 'customer_rejected_shipment', 'courier_company_lost_shipment', 'courier_company_lost_shipment', 'returned_other_reason', 'created_by', 'created_by', 'processed_by', 'created_at', 'updated_at'
	];

	public function user(){
		return $this->belongsTo('App\User','created_by');
	}
}
