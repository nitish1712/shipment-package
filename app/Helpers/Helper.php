<?php

if (!function_exists('success')) {
    function success($message, $status_code, $data = array())
    {
        if (empty($data) && !is_numeric($data)) {
            $data = json_encode(new stdClass());
        }
        return response()->json(['message' => $message, 'data' => $data], $status_code);
    }
}

if (!function_exists('failed')) {
    function failed($message, $status_code, $data = array())
    {
        if (empty($data)) {
            $data = json_encode(json_decode("{}"));
        }

        return response()->json(['message' => $message, 'errors' => $data], $status_code);
    }
}