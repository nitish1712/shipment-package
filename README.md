Steps for Project deployment : 

- [php version >= 7.3]
- [mysql version >= 5.7]
- [clone the repositor].
- [Run command composer install]
- [cp .env.example .env]
- [import db_sample.sql in mysql]
- [php artisan server]
- [access the API's via postman with the collection and environment shared (kindly change the url in case of differennt local url)]
- [php artisan passport:install]
